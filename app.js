const express = require('express')

const fs = require('fs');

const requestID = require('express-request-id');

const app = express();

const { port, statuscode } = require('./config');

const router = require('./routers.js');
const { request } = require('http');


middleWareLogging = (request, response, next) => {                                                                                                      //creating a middleware for log file
    let log = {
        "URL": request.url,
        "request ID": request.id,
        "Date": new Date()
    };                                                                                                                                               //requesting url to store
    fs.appendFile('./logData.log', JSON.stringify(log, null, 2), (err) => {                                                                            // creating and appending file 
        if (err) {
            next({});
        }
        else {
            console.log("Log added");
            next();                                                                                                           //consoles if log added.
        }
    })

};
app.use(requestID());
app.use(middleWareLogging);


app.use(router);

app.use((request,response) => {
    response.status(400).send(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Wrong URL. Input URL as per need.</h1>
          <h3>Try using the following:</h3>
          <h5>/IPL/1    for finding the Number of matches played per year for all the years in IPL.</h5>
          <h5>/IPL/2    for finding the Number of matches won per team per year in IPL.</h5>
          <h5>/IPL/3    for finding the Extra runs conceded per team in the year 2016.</h5>
          <h5>/IPL/4    for finding the Top 10 economical bowlers in the year 2015.</h5>
          <h5>/IPL/5    for finding the number of times each team won the toss and also won the match.</h5>
          <h5>/IPL/6    for finding a player who has won the highest number of Player of the Match awards for each season.</h5>
          <h5>/IPL/7    for finding the strike rate of a batsman for each season.</h5>
          <h5>/IPL/8    for finding the highest number of times one player has been dismissed by another player.</h5>
          <h5>/IPL/9    for finding the bowler with the best economy in super overs.</h5>
      </body>
    </html>
    `)
});

app.use((err, request, response, next) => {                                                                                            //error handling middleware

    console.log(err);
    const stat = err.statuscode || 500;                                                                                         //changes code to 500, when error comes.
    response.status(stat);
    response.json({ "Error": err.message }).end();

});

app.listen(port, (err) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log("Server running");
    }
});

