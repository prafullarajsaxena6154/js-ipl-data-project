const express = require('express');
const router = express.Router()

router.get('/IPL/3', (request, response, next) => {
    console.log('ljkjlkl');
    let result = require('./src/server/3-extra-runs-conceded.cjs');
    console.log(result());
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});


router.get('/IPL/1', (request, response, next) => {
    let result = require('./src/server/1-matches-per-year.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});


router.get('/IPL/2', (request, response, next) => {
    let result = require('./src/server/2-matches-won-per-team-per-year.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});


router.get('/IPL/4', (request, response, next) => {
    let result = require('./src/server/4-top-10-economical-bowlers.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});

router.get('/IPL/5', (request, response, next) => {
    let result = require('./src/server/5-no-of-times-toss-win.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});


router.get('/IPL/6', (request, response, next) => {
    let result = require('./src/server/6-highest-no-POTM.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});

router.get('/IPL/7', (request, response, next) => {
    let result = require('./src/server/7-strike-rate-batsmen.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});


router.get('/IPL/8', (request, response, next) => {
    let result = require('./src/server/8-highest-dismissal.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});


router.get('/IPL/9', (request, response, next) => {
    let result = require('./src/server/9-best-economy-superovers.cjs');
    result().then((ob) => {
        response.json(ob).end();
    }).catch((err) => {
        next();
    });

});

router.get('/IPL', (request, response, next) => {
    response.send(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Welcome to the IPL window. Input URL as per need.</h1>
          <h3>Try using the following:</h3>
          <h5>/IPL/1    for finding the Number of matches played per year for all the years in IPL.</h5>
          <h5>/IPL/2    for finding the Number of matches won per team per year in IPL.</h5>
          <h5>/IPL/3    for finding the Extra runs conceded per team in the year 2016.</h5>
          <h5>/IPL/4    for finding the Top 10 economical bowlers in the year 2015.</h5>
          <h5>/IPL/5    for finding the number of times each team won the toss and also won the match.</h5>
          <h5>/IPL/6    for finding a player who has won the highest number of Player of the Match awards for each season.</h5>
          <h5>/IPL/7    for finding the strike rate of a batsman for each season.</h5>
          <h5>/IPL/8    for finding the highest number of times one player has been dismissed by another player.</h5>
          <h5>/IPL/9    for finding the bowler with the best economy in super overs.</h5>
      </body>
    </html>
    `).end();
})

router.get('/', (request, response) => {
    response.status(400).send(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Input URL as /IPL for further functions.</h1>
          </body>
        </html>
        `).end();
})

module.exports = router;