// const fS = require('fs');
const csvToJson = require('csvtojson');
const path = require('path');

const csvFile = path.join(__dirname,'../data/matches.csv');
// const jsonFile = '../public/6-highest-no-POTM.json';                                                        //JSON file path

const highestnoPOTM = () => {                                                                               //function for operation
    return csvToJson().fromFile(csvFile).then((matchesArray) => {
        let result = matchesArray.reduce((resultObj, ele) => {
            if (ele.season in resultObj) {
                if (ele.player_of_match in resultObj[ele.season]) {                                                           //checking if player already present in resultObj 
                    resultObj[ele.season][ele.player_of_match] += 1;
                }
                else {
                    resultObj[ele.season][ele.player_of_match] = 1;                                                         //if not present, entry is created
                }
            }
            else {
                resultObj[ele.season] = {};
            }
            // let tempSortArray = Object.entries(resultObj);
            // console.log(tempSortArray);
            return resultObj;
        }, {});
        result = Object.entries(result);
        result = result.map((ele) => {                                                                              //sorting using the entries function, on the basis of number of player_of_match
            ele[1] =Object.fromEntries((Object.entries(ele[1])
            .sort((a,b) => {
                return b[1] - a[1];
            }).slice(0,1)));
            return ele;
        })
        result = Object.fromEntries(result);                                                                                //converted back to object
        result = JSON.stringify(result);
        return result;

        // fS.writeFile(jsonFile,result,(err) => {                                                             //written to JSON file
        //     if(err){
        //         console.error(err);
        //     }
        // });
    })
}
module.exports = highestnoPOTM;