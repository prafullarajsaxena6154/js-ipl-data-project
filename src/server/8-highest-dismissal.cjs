// const fs = require('fs');
const path = require('path');
const csvFile_Deliveries = path.join(__dirname,'../data/deliveries.csv');
const csv = require('csvtojson')

function highest_Dismissal() {                                                                                  //function with the solution to problem

    return csv().fromFile(csvFile_Deliveries).then((deliveriesObj) => {
        let result = deliveriesObj.reduce((resultObj, ele) => {
            if (ele.player_dismissed !== '') {                                                                  //checks if player dismissed valid
                if (ele.bowler in resultObj) {                                                                  //check if bowler present

                    if (ele.player_dismissed in resultObj[ele.bowler]) {                                        //checks if player dismissed by bowler
                        resultObj[ele.bowler][ele.player_dismissed] += 1;
                    }
                    else {
                        resultObj[ele.bowler][ele.player_dismissed] = +1;
                    }

                }
                else {
                    resultObj[ele.bowler] = {};
                    resultObj[ele.bowler][ele.player_dismissed] = +1
                }
            }
            return resultObj;
        }, {});
        let max = [0];                                                                              
        result = Object.entries(result).filter((ele) => {                                                      //for finding the max times dissmissed
            Object.keys(ele[1]).map((element) => {
                tempMax = Math.max(ele[1][element]);
                if (tempMax > max[0]) {
                    max = [tempMax, ele[0], element];                                                           //storing who dismissed whom for maximum instances
                }
                return element;
            });
            return true;
        });
        let finResult = {
            bowler: max[1],
            batsman: max[2],
            dismissed: max[0]
        };
        finResult = JSON.stringify(finResult); 
        
        return finResult;                                                                 //conversion to JSON and storing
        // fs.writeFile('../public/8-highest-dismissal.json',finResult,(err) => {
        //     if(err){
        //         console.error(err);
        //     }
        // })
    });
}
module.exports = highest_Dismissal;
