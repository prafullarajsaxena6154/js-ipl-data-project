// const fS = require('fs');
const path = require('path');
const csvToJson = require('csvtojson');

const csvFile = path.join(__dirname,'../data/matches.csv');
// const jsonFile = '../public/2-matches-won-per-team-per-year.json';

const matchesWonPerTeam = () => {
    return csvToJson().fromFile(csvFile).then((matchesArray) => {                                  //reading data from csvFile address, using .then(). Stores read data into matchesArray

        let result = matchesArray.reduce(((resultObject, ele) => {
            if (ele.season in resultObject) {                                               //checks if season is present in resultant Object
                if (ele.season in resultObject) {
                    if (ele.winner in resultObject[ele.season]) {                           //checks if winning team is present in respective season
                        resultObject[ele.season][ele.winner] += 1;                          //adds number of wins if present
                    }
                    else {
                        resultObject[ele.season][ele.winner] = 1;                           //adds a new entry if not present
                    }
                }
            }
            else {
                resultObject[ele.season] = {};                                              //adds a new object entry to store various winning teams names in a season
            }
            return resultObject;
        }), {})
        result = JSON.stringify(result);                                                    //writing JSON file
        
        return result;
        // fS.writeFile(jsonFile, result, (err) => {
        //     if (err) {
        //         console.error(err)
        //     }
        // })
    });
}

module.exports = matchesWonPerTeam;                                                                        //function called