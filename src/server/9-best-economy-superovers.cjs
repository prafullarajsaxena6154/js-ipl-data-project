const path = require('path');
const csvtojson = require("csvtojson");
const csvFile_Deliveries = path.join(__dirname,"../data/deliveries.csv");


function bestEconomySO() {
   return csvtojson().fromFile(csvFile_Deliveries).then((deliveries) => {

        let result = {};
        let super_Over_Bowlers = deliveries.filter((value) => {             //finds and stores super over bowlers object.
            return value.is_super_over !== "0";
        });
        // console.log(super_Over_Bowlers);
        result = super_Over_Bowlers.reduce((resultObj, ele) => {                             //finds and stores, balls bowled and runs conceded by bowlers.
            if (ele.bowler in resultObj) {
                resultObj[ele.bowler]['runs_given'] += +(ele.total_runs);
                resultObj[ele.bowler]['balls_bowled'] += +1;
            } else {
                resultObj[ele.bowler] = {};
                resultObj[ele.bowler]['runs_given'] = +(ele.total_runs);
                resultObj[ele.bowler]['balls_bowled'] = +1;
            }
            return resultObj;
        }, {});
        let economy_Obj = {};                                               //finds and stores economy of each bowler.
        result = Object.entries(result).map((ele) => {
            economy_Obj[ele[0]] = {};

            const economy = (ele[1].runs_given / ele[1].balls_bowled) * 6;
            economy_Obj[ele[0]] = +economy.toFixed(2);
            return ele;
        })
        // console.log(result);
        // console.log(economy_Obj);


        let top_Economy = Object.entries(economy_Obj).sort((a, b) => a[1] - b[1]).slice(0, 1); //this stores the top economy by sorting on the basis of economy
        top_Economy = Object.fromEntries(top_Economy);
        top_Economy = JSON.stringify(top_Economy);
       
        return top_Economy;
        // fS.writeFile("../public/9-best-economy-superover.json",top_Economy,(err) => {
        //     if(err){
        //         console.error(err);
        //     }
        // });
    });
}
module.exports = bestEconomySO;