// const fS = require('fs');
const path = require('path');
const csvToJson = require('csvtojson');

const csvFileDeliveries = path.join(__dirname,'../data/deliveries.csv');
const csvFileMatches = path.join(__dirname,'../data/matches.csv');
// const jsonFile = '../public/4-top-10-economical-bowlers.json';

const top10Economical = () => {                                                                                 //function for problem 4
    return csvToJson().fromFile(csvFileMatches).then((matchesArray) => {                                               //converting matches.csv file, then storing data in matchesArray
        let idArray = matchesArray.filter((ele) => {                                                            //storing IDs from only 2015 season
            return ele.season === '2015';
        }).map((ele) => {
            return ele.id;
        });

        return csvToJson().fromFile(csvFileDeliveries).then((deliveriesArray) => {                                     //converting deliveries.csv file, then storing data in deliveriesArray
            let result = deliveriesArray.reduce((resultObj, ele) => {
                if (idArray.includes(ele.match_id)) {
                    if (ele.wide_runs === "0" || ele.noball_runs === "0" || ele.penalty_runs === "0") {         //checking if number of balls should be incremented
                        if (resultObj[ele.bowler]) {                                                            //checking if bowler in resultObj object
                            resultObj[ele.bowler].balls += 1;                                                   //incrementing number of balls and total runs in that ball
                            resultObj[ele.bowler].runs += +ele.total_runs;
                        }
                        else {
                            resultObj[ele.bowler] = {
                                balls: +1,                                                                      //declaring an object within bowler Object
                                runs: +ele.total_runs
                            }
                        }
                    }
                    else {
                        if (resultObj[ele.bowler]) {
                            resultObj[ele.bowler].balls += 0;
                            resultObj[ele.bowler].runs += +ele.total_runs;
                        }
                        else {
                            resultObj[ele.bowler] = {
                                balls: +0,
                                runs: +ele.total_runs
                            }
                        }
                    }
                }
                return resultObj;                                                                              //returns an object with bowler name, totals runs and balls
            }, {});
            let finData = (Object.entries(result).map((ele) => {                                               // final data to be written in JSON file,
                return [ele[0], ((ele[1].runs / ele[1].balls) * 6).toFixed(2)];                                // final data is being converted to economy
            }).sort((a, b) => {                                                                                // sorting the array
                return a[1] - b[1];     
            })).slice(0, 10);                                                                                  // picking top 10
            finData = Object.fromEntries(finData);                                                          
            finData = JSON.stringify(finData);
            // console.log(finData);
            
            return finData;
            // fS.writeFile(jsonFile, finData, (err) => {                                                          //writing to JSON file
            //     if (err) {
            //         console.error(err);
            //     }
            // })
        });
    })
}
module.exports = top10Economical;