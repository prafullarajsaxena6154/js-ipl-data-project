// const fS = require('fs');
const path = require('path');
const csvToJson = require('csvtojson');

const csvFile = path.join(__dirname,'../data/matches.csv');
// const jsonFile = '../public/5-no-of-times-toss-win.json';

const noOfTimesTossWin = () => {
    
    return csvToJson().fromFile(csvFile).then((matchesObj) =>{                                             //converting data and storing in matchesObj
        let result = matchesObj.reduce((resultObj,ele) => {
            if(ele.toss_winner === ele.winner){                                                     //checking if winner equal to toss winner
                if(ele.winner in resultObj)
                {
                    resultObj[ele.winner] += 1;
                }
                else{                                                                               //storing new winner if not present
                    resultObj[ele.winner] = 1;
                }
            }
            return resultObj;
        },{});
        result = JSON.stringify(result);                                                            //converting result data to string form
        return result;
        // fS.writeFile(jsonFile,result,(err) => {                                                     //writing to JSON file. 
        //     if(err){
        //         console.error(err);
        //     }
        // })
    });
}

module.exports = noOfTimesTossWin;