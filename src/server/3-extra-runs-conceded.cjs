// const fS = require('fs');
const path = require('path');
const csvToJson = require('csvtojson');

const csvFileDeliveries = path.join(__dirname,'../data/deliveries.csv');
const csvFileMatches = path.join(__dirname,'../data/matches.csv');
// const jsonFile = '../public/3-exta-runs-conceded.json';

function extraRunsConceded() {
    return csvToJson().fromFile(csvFileMatches).then((matchesArray) => {          //reading data from csvFile address, using .then(). Stores read data into matchesArray
        let idArray = matchesArray.filter((ele) => {                       //filtering storing IDs of matches in 2016
            return ele.season === '2016'
        }).map((ele) => {
            return ele.id;
        });

        return csvToJson().fromFile(csvFileDeliveries).then((deliveriesArray) => {         //reading data from csvFile address, using .then(). Stores read data into deliveriesArray
            let result = deliveriesArray.reduce((resultObject, ele) => {            //stores the result array in result
                if (idArray.includes(ele.match_id)) {                               //checks for match IDs in array with IDs of 2016 matches
                    if (ele.bowling_team in resultObject) {                         //checks if team data already exists
                        resultObject[ele.bowling_team] += +ele.extra_runs;
                    }
                    else {
                        resultObject[ele.bowling_team] = +ele.extra_runs;
                    }
                }
                return resultObject;
            }, {});
            result = JSON.stringify(result);
            // console.log(result);
            return result;
            // fS.writeFile(jsonFile,result,(err) => {                                //writing to JSON file
            //     if(err){
            //         console.error(err);
            //     }
            // });
        });
    });
}
module.exports = extraRunsConceded;