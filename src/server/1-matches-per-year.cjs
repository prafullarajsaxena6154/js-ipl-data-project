// const fS = require('fs');                                                   //calling fS module
const csvToJson = require('csvtojson');                                     //calling csvtojson module
const path = require('path');
const csvFile = path.join(__dirname,'../data/matches.csv');                                      //file path of matches.csv file
// const jsonFile = path.join(__dirname'../public/1-matches-per-year.json');                       //file where we store the data

const matchesPerYear = () => {                                              //function for the problem 1.
    
   return csvToJson()
    .fromFile(csvFile)
    .then((matchesArray) =>{                                                //using .then for asynchronous function fromFile
        let result = matchesArray.reduce((resultObject, ele) => {
            if(ele.season in resultObject){                                 //checks if season is already present in resultant data, if yes adds instance, if no adds a new key with season name
                resultObject[ele.season] +=1;
            }
            else{
                resultObject[ele.season] = 1;
            }
            return resultObject;
        },{});                                                              //assigning empty object value to accumulator object resultObject
        // console.log(result);
        result = JSON.stringify(result);                                    //converting to string for storing in JSON file
        
        return result;
        // fS.writeFile(jsonFile,result,(err) => {                             //writing to the JSON file
        //     if(err){
        //         console.error(err);
        //     }
        // })
    });
}
module.exports = matchesPerYear;                                                           //function called



