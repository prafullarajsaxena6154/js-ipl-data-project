// const fs = require('fs');
const path = require('path');
const csvFile_Matches = path.join(__dirname, '../data/matches.csv');
const csvFiles_Deliveries = path.join(__dirname, '../data/deliveries.csv');
const csv = require('csvtojson')

function strike_Rate() {
   return csv().fromFile(csvFile_Matches).then(matchesObj => {
      let arrayofIds = matchesObj.reduce((resultObj, ele) => {
         if (!(ele.id in resultObj)) {
            resultObj[ele.id] = ele.season;                                                                                                        //storing id with year
         }
         return resultObj;
      }, {});
      return csv().fromFile(csvFiles_Deliveries).then(deliveriesObj => {
         let idsRunsBalls = deliveriesObj.reduce((resultObj, ele) => {
            let balls = ((ele.noball_run !== '0' || ele.legbye_run !== '0' || ele.bye_run !== '0' || ele.batsman_run !== '0') ? +1 : +0);          //checking if valid ball
            let runs = +ele.batsman_runs;
            if (ele.batsman in resultObj) {
               if (arrayofIds[ele.match_id] in resultObj[ele.batsman]) {
                  resultObj[ele.batsman][arrayofIds[ele.match_id]]['balls_played'] += balls;                                                       //calculating balls played

                  resultObj[ele.batsman][arrayofIds[ele.match_id]]['runs_scored'] += runs;                                                         //calculating runs
               }
               else {
                  resultObj[ele.batsman][arrayofIds[ele.match_id]] = {                                                                             //creating Object
                     balls_played: balls,
                     runs_scored: runs
                  }
               }
            }
            else {
               resultObj[ele.batsman] = {};                                                                                                        //creating Object

               resultObj[ele.batsman][arrayofIds[ele.match_id]] = {
                  balls_played: balls,
                  runs_scored: runs
               }
            }
            return resultObj;
         }, {});

         idsRunsBalls = Object.fromEntries((Object.entries(idsRunsBalls)).map((ele) => {                                                            //Hof for strike rate calculations
            ele[1] = Object.entries(ele[1]).map((element) => {
               element[1] = ((element[1].runs_scored / element[1].balls_played) * 100).toFixed(2);
               return element;
            })
            return ele;
         }))
         idsRunsBalls = JSON.stringify(idsRunsBalls, null, 2);

         return idsRunsBalls;
         // fs.writeFile('../public/7-strike-rate_batsmen.json',idsRunsBalls,(err) => {                                                               //writing to JSON file
         //    if(err){
         //       console.error(err);
         //    }
         // })
      })
   });

}
module.exports = strike_Rate;

