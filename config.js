const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    port: process.env.PORT,
    statuscode: process.env.STATUSCODE,
};
